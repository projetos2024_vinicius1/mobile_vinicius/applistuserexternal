import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Lista from './src/components/lista.js';
import Info from './src/components/info.js';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Lista'>
        <Stack.Screen name='Lista' component={Lista} options={{ title: 'Home' }}/>
        <Stack.Screen name='Info' component={Info} options={{ title: 'Informações' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
