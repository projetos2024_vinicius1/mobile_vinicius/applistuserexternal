import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";

const Info = ({ route }) => {
    const { task } = route.params;

    return (
        <View style={styles.container}>
            <Text style={{ fontSize: '30px', marginBottom:'5px' }}>Detalhes do Usuário: "{task.name}":</Text>
            <View>
                <Text style={styles.textoConteudo}>Dados Pessoais:</Text>
                <Text>Nome: {task.name}</Text>
                <Text>Apelido: {task.username}</Text>
                <Text>Email: {task.email}</Text>
                <Text>Telefone: {task.phone}</Text>
                <Text style={{marginBottom: '5px'}}>Website: {task.website}</Text>
                <Text style={styles.textoConteudo}>Endereço:</Text>
                <Text>Rua: {task.address.street}</Text>
                <Text>Suíte: {task.address.suite}</Text>
                <Text>Cidade: {task.address.city}</Text>
                <Text style={{marginBottom:'5px'}}>Zipcode: {task.address.zipcode}</Text>
                <Text style={styles.textoConteudo}>Geo:</Text>
                <Text>Latitude: {task.address.geo.lat}</Text>
                <Text>Longitude: {task.address.geo.lng}</Text>
            </View>
        </View>
    )

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'top',
        alignItems: 'center',
    },
    textoConteudo: {
        fontSize: '20px',
    },
    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
        weight: '20px',
    }
});

export default Info;

