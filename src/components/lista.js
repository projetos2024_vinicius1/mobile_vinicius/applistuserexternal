import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, FlatList, ScrollView } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "axios";

const Lista = ({ navigation }) => {

    const [users, setUsers] = useState([]);
    useEffect(() => {
        getUsers();
    }, []);

    async function getUsers() {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            setUsers(response.data);
        }
        catch (error) {
            console.error(error);
        }
    };

    const taskPress = (task) => {
        navigation.navigate('Info', { task });
    }

    return (
        <View style={styles.container}>
            <Text style={{ marginBottom: '7px', fontSize: '30px' }}> Lista de Usuários</Text>
            <FlatList
                data={users}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                    <View>
                        <TouchableOpacity onPress={() => taskPress(item)} style={styles.menu}>
                            <Text>
                                {item.name}
                            </Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
        weight: '20px',
    }
});


export default Lista;

